import discord
import os
import io
from dotenv import load_dotenv
from config import Data
from random import shuffle

load_dotenv()

bot = discord.Client()
data = Data("data.json")


def make_pairs(names):
    return [
        (name.id, names[(i + 1) % len(names)].id)
        for i, name in enumerate(names)
    ]


def get_path(name):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), name)


async def send_file(table, user):
    file_path = get_path('data.txt')
    fichier = open(file_path, 'w')
    for send, gift in table:
        gift_user = await bot.fetch_user(gift)
        fichier.write(f"{send} \t {gift_user.name}\n")
    fichier.close()
    await user.send("le ficheir de donnée", file=discord.File(file_path))
    os.remove(file_path)


async def send_user(table, chan):
    for send, gift in table:
        try:
            guild = chan.guild
            gifter = await guild.fetch_member(gift)
            user = await bot.fetch_user(send)
            if gifter.nick is None:
                nick = gifter.name
            else:
                nick = gifter.nick
            await user.send(f"tu dois offrir un cadeau à <@{gift}> : nickname = {nick}, name = {gifter.name}")
        except discord.HTTPException:
            await chan.send(
                f"<@{send}>, ouvres tes dm, sinon je ne peux pas t'envoyer de message"
            )


async def delib(users, checker, chan, msg_id):
    if len(users) < 1:
        await checker.send("pas assez d'utilisateurs")
        msg = await chan.fetch_message(msg_id)
        await msg.remove_reaction('✅', checker)
        return False
    else:
        shuffle(users)
        pairs = make_pairs(users)
        await send_file(pairs, checker)
        await send_user(pairs, chan)
        return True


@bot.event
async def on_raw_reaction_add(payload: discord.RawReactionActionEvent):
    if payload.emoji.name == '✅' and payload.message_id == data.data[str(payload.guild_id)]:
        if payload.member.guild_permissions.administrator:
            chan = bot.get_guild(payload.guild_id).get_channel(
                payload.channel_id)
            msg = await chan.fetch_message(payload.message_id)
            for reactions in msg.reactions:
                if(reactions.emoji == '✋'):
                    list = []
                    async for user_react in reactions.users():
                        if user_react.id != bot.user.id:
                            list.append(user_react)
            if await delib(list, payload.member, chan, payload.message_id):
                data.data[str(payload.guild_id)] = 0
                data.write()
        else:
            if payload.user_id != bot.user.id:
                await bot.get_user(payload.user_id).send("Tu n'as pas les perms pour terminer les inscriptions")


@bot.event
async def on_message(message: discord.Message):
    if message.content.startswith("/ss start"):
        if message.author.guild_permissions.administrator:
            await message.delete()
            await start_ss(message.channel, message.author)
        else:
            await message.channel.send(f"Tu ne peux lancer le Secret Santa {message.author.mention}")


async def start_ss(chan: discord.TextChannel, user: discord.user):
    if not str(chan.guild.id) in data.data:
        data.data[str(chan.guild.id)] = 0
    if data.data[str(chan.guild.id)] != 0:
        await chan.send(f"Il y en a déjà de commencé dans le serveur {user.mention}")
    else:
        msg: discord.message = await chan.send("Le Secret Santa débute réaggissez avec ✋ pour participer")
        await msg.add_reaction('✋')
        await msg.add_reaction('✅')
        data.data[str(chan.guild.id)] = msg.id
        data.write()


@bot.event
async def on_ready():
    print('Logged in as {0} ({0.id})'.format(bot.user))
    print('------')

bot.run(os.getenv("TOKEN"))
