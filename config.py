import os
import json


class Data:
    def __init__(self, file_name) -> None:
        self.__config_file = self.get_path(file_name)
        if not os.path.isfile(self.__config_file):
            print(f"config {file_name} doesn't exist, copying template!")
            file = open(self.__config_file, "w")
            file.write("{}")
            file.close()
        file = open(self.__config_file)
        self.data = json.load(file)
        file.close()

    def get_path(self, name):
        return os.path.join(os.path.dirname(os.path.realpath(__file__)), name)

    def write(self) -> None:
        with open(self.__config_file, 'w', encoding='utf-8') as data_file:
            json.dump(self.data, data_file, ensure_ascii=False, indent=4)

